<?php
require_once('vendor/autoload.php');

$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();

$mySforceConnection = new SforceEnterpriseClient();
$mySoapClient = $mySforceConnection->createConnection("enterprise.wsdl.xml");
$mylogin = $mySforceConnection->login(getenv('FORCE_USER'), getenv('FORCE_PASSWORD'));
