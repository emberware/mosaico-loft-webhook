<?php
abstract class IFQ_Salesforce_Item{
    protected $paramsCsv = array();
    protected $paramsApi = array();
    protected $returnType = array(
        'api' => 'obj',
        'csv' => 'array',
    );
    protected static $itemTypes = array();
    protected function output($data,$type = 'api'){
        $params = 'params'.ucfirst($type);
        $return = false;
        if(!empty($this->$params)){
            if($this->returnType[$type] == 'obj'){
                $return = new stdClass();
                foreach($this->$params as $localKey => $returnKey){
                    if(isset($data[$localKey])){
                        $return->$returnKey = isset($data[$localKey])?$data[$localKey]:'';
                    }
                }
            }elseif($this->returnType[$type] == 'array'){
                $return = array();
                foreach($this->$params as $localKey => $returnKey){
                    $return[$returnKey] = isset($data[$localKey])?$data[$localKey]:'';
                }
            }
        }
        return $return;
        
    }
    
    protected abstract function buildItem($itemId);
    
    public function getItem($itemId,$type = 'api'){
        $data = $this->buildItem($itemId);
        return $this->output($data,$type);
    }
    
    public static function addItemType($type,$class){
        static::$itemTypes[$type] = $class;
    }
    
    public static function factory($type){
        if(!isset(static::$itemTypes[$type]) && class_exists(static::$itemTypes[$type])){
            throw new Exception('Non valid item type');
        }
        else{
            return new static::$itemTypes[$type]();
        }
    }
    
    public static function isValid($item_id){
        return true;
    }
    public function getId($item_id){
	$Id = get_metadata($this->metaType,$item_id,'_ifq_salesforce_id',true);
	if(empty($Id)){
            $sforce = IFQ_Salesforce_Update::instance();
            $res = $sforce->get($item_id.":SP",$this);
            if(!empty($res)&&is_array($res)){
                $Id = $res[0]->Id;
                update_metadata($this->metaType,$item_id,'_ifq_salesforce_id',$Id);
            }
	}
        return $Id;
    }
}
