<?php 
class IFQ_Salesforce_Subscription extends IFQ_Salesforce_Item{
    
    public $paramsCsv = array(
        'subscription_id' => 'subscription_id',
        'user_id' => 'subscriber_id',
        'original_order_id' => 'original_order_id',
        'product_name' => 'product_name',
        'product_id' => 'product_id',
        'end_date' => 'end_date',
        'start_date' => 'start_date',
        'status' => 'status',
        'amount_spent' => 'amount_spent',
        'fonte' => 'fonte',
        
    );
    protected $paramsApi = array(
        'subscription_id' => 'ID_abbonamento__c',
        'user_id' => 'Codice_Abbonato__c',
        'original_order_id' => 'Codice_original_order__c',
        'product_name' => 'Name',
        //'product_id' => 'Order_Product__c', ???
        'end_date' => 'Data_Fine__c',
        'start_date' => 'Data_Inizio__c',
        'status' => 'Stato_Abbonamento__c',
        //'amount_spent' => 'Amount_Spent',
        'fonte' => 'Fonte__c',
        'external_id' => 'KEY_Abbonamento__c',
        'salesforce_order_id' =>'ID_original_order__c',
        'salesforce_customer_id' =>'ID_abbonato__c',
         
    );
    
    public $upsertKey = 'KEY_Abbonamento__c';
    public $type = 'Nome_Abbonamento__c';
    public $metaType = 'post';
    protected $substatusToSalesStatus = array(
	'active' => 'in corso',
	'pending-cancel' => 'in corso',
	'cancelled' => 'annullato',
	'pending' => 'in attesa',
	'on-hold' => 'sospeso',
	'expired' => 'terminato',
	'trash' => 'annullato',
	);    
    protected function buildItem($subscription_id){
        $subscription = wcs_get_subscription($subscription_id);
        $data = array(
            'subscription_id' => $subscription->id,
            'external_id' => $subscription->id.":SP",
            'user_id' => $subscription->get_user_id().":SP",
            'start_date' => date('Y-m-d\TH:i:sP',strtotime($subscription->get_date('start'))),
            'status' => $this->substatusToSalesStatus[$subscription->get_status()],
            'fonte' => 'SP',
        );
        $end_date = $subscription->get_date('end');
        if(empty($end_date)){
            if($subscription->get_status() == 'active'){
                $end_date = $subscription->get_date('next_payment');
            }elseif($subscription->get_status() == 'pending-cancel'){
                $end_date = $subscription->get_date('end_of_prepaid_term');
            }
        }
        
        $data['end_date'] = date('Y-m-d\TH:i:sP',strtotime($end_date));
        foreach($subscription->get_items() as $item){
                $product = $subscription->get_product_from_item($item);
                if(!empty($product)){
			$product_id = $product->id;
                        $data['product_id'] = $product->id.":SP";
                        $data['product_name'] = substr($product->get_title(),0,80);
                }
        }
        $orders = $subscription->get_related_orders('all', array('parent,renewal'));
        $amount = 0;
        foreach($orders as $order){
            foreach($order->get_items() as $item){
                if($item['product_id'] == $product_id){
                    $amount +=$item['line_total'];
                }
            }
        }
        $data['amount_spent'] =round( $amount, wc_get_price_decimals() );
        if($subscription->get_status() == 'active' || $subscription->get_status() == 'pending-cancel' || (time()-strtotime($end_date) < YEAR_IN_SECONDS)){
            $original_order_id = $subscription->get_last_order('ids','parent');
            if(empty($original_order_id)){
                $original_order_id = array_shift($subscription->get_related_orders('ids','renewal'));
            }
            $data['original_order_id'] = $original_order_id.":SP";
        }
        else{
            $data['original_order_id'] = "";
        }
        if(!empty($original_order_id)){
            $orderType = IFQ_Salesforce_Item::factory('order');
            $salesforceOrderId = $orderType->getId($original_order_id);
            if(!empty($salesforceOrderId)){
                $data['salesforce_order_id'] = $salesforceOrderId;
            }
        }
	$userType = new IFQ_Salesforce_User();
	$salesforceUserId = $userType->getId($order->customer_user);
        if(!empty($salesforceUserId)){
            $data['salesforce_customer_id'] = $salesforceUserId;
        }
        return $data;
    }
}
IFQ_Salesforce_Item::addItemType('subscription','IFQ_Salesforce_Subscription');
