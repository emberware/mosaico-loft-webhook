<?php 
class IFQ_Salesforce_Order_Item extends IFQ_Salesforce_Item{
    
    public $paramsCsv = array(
        'order_item_id' => 'order_item_id',
        'order_id' => 'order_id',
        'product_id' => 'product_id',
        'quantity' => 'quantity',
        'fonte' => 'fonte',
        
    );
    protected $paramsApi = array(
        'order_item_id' => 'ID_Order_Item__c',
        'order_id' => 'Codice_Ordine__c',
        'product_id' => 'Codice_Prodotto__c',
        'quantity' => 'Quantity',
        'fonte' => 'Fonte__c',
        'external_id' => 'KEY_ProdottoOrdinato__c',
        'external_product_id' =>'KEY_ProdottoOrdinato__c',
        'salesforce_order_id' => 'OrderId',
        'salesforce_pricebookentry' => 'PricebookEntryId',
        'salesforce_product_id' => 'Product2Id',
        'unit_price' =>'UnitPrice',
        
    );
    
    public $upsertKey = 'KEY_ProdottoOrdinato__c';
    public $type = 'OrderItem';
    public $metaType = 'order_item';
    protected function buildItem($order_item_id){
        global $wpdb;
        $get_items_sql  = $wpdb->prepare( "SELECT order_item_id, order_item_name, order_item_type, order_id FROM {$wpdb->prefix}woocommerce_order_items WHERE order_item_id = %d AND order_item_type ='line_item' ", $order_item_id );       
        $line_item     = $wpdb->get_row( $get_items_sql );
	if(!IFQ_Salesforce_Order::isValid($line_item->order_id)){
		return false;
	}
        $orderType = IFQ_Salesforce_Item::factory('order');
        $product_id = get_metadata( 'order_item', $order_item_id, '_product_id',true );
        $qty = get_metadata( 'order_item', $order_item_id, '_qty', true );
        $data = array(
            'order_item_id' => $line_item->order_item_id,
            'external_id' => $line_item->order_item_id.":SP",
            'order_id' => $line_item->order_id.":SP",
            'product_id' => $product_id.":SP",
            'quantity' =>(empty($qty)?1:$qty),
            'fonte' => 'SP',
       
            
        );
        $salesforceOrderId = $orderType->getId($line_item->order_id);
        if(!empty($salesforceOrderId)){
            $data['salesforce_order_id'] = $salesforceOrderId;
        }
        $productItem = IFQ_Salesforce_Item::factory('product');
        $salesforceProductId = $productItem->getId($product_id);
        if(!empty($salesforceProductId)){
            $data['salesforce_product_id'] = $salesforceProductId;
        }
        $salesforcePricebookEntryId = $productItem->getPricebookEntryId($product_id);
        if(!empty($salesforcePricebookEntryId)){
            $data['salesforce_pricebookentry'] = $salesforcePricebookEntryId;
        }
        
        $price = get_metadata( 'order_item', $order_item_id, '_line_total',true )+get_metadata( 'order_item', $order_item_id, '_line_tax',true );
        $unitPrice = $price/$qty;
        $data['unit_price'] = $unitPrice?$unitPrice:0;
        return $data;
    }
    public static function isValid($item_id) {
        $on_salesforce = get_metadata( 'order_item', $order_item_id, '_ifq_salesforce_order_item_loaded',true );
        if(!empty($on_salesforce) && intval($on_salesforce) == 1){
            return false;
        }
        return true;
    }
}
IFQ_Salesforce_Item::addItemType('order-item','IFQ_Salesforce_Order_Item');

