<?php

class IFQ_Salesforce_User extends IFQ_Salesforce_Item{
    
    public $paramsCsv = array(
        'user_id' => 'person_id',
        'last_name' => 'last_name',
        'first_name' => 'first_name',
        'nickname' => 'nickname',
        'company' => 'nome_società',
        'codice_fiscale' => 'codice_fiscale',
        'data_di_nascita' => 'data_di_nascita',
        'email' => 'email',
        'telefono' => 'telefono',
        'status_subscription' => 'status',
        'studente' => 'studente',
        'registration_date' => 'registration_date',
        'consenso_marketing' => 'consenso_marketing',
        'consenso_privacy' => 'consenso_privacy',
        'consenso_vendita' => 'consenso_vendita',
        'last_subscription_end_date' => 'end_date',
        'first_subscription_start_date' => 'start_date',
        'membership_level' => 'membership_level',
        'membership_level_name' => 'membership_level_name',
        'billing_address_1' => 'indirizzo_1_fatturazione',
        'billing_address_2' => 'indirizzo_2_fatturazione',
        'billing_city' => 'comune_fatturazione',
        'billing_postcode' => 'cap_fatturazione',
        'billing_state' => 'provincia_fatturazione',
        'billing_regione' => 'regione_fatturazione',
        'billing_country' => 'nazione_fatturazione',
        'shipping_address_1' => 'indirizzo_1_spedizione',
        'shipping_address_2' => 'indirizzo_2_spedizione',
        'shipping_city' => 'comune_spedizione',
        'shipping_postcode' => 'cap_spedizione',
        'shipping_state' => 'provincia_spedizione',
        'shipping_regione' => 'regione_spedizione',
        'shipping_country' => 'nazione_spedizione',
        'source' => 'sorgente', // = Shop
        'sex' => 'sesso',
        'fonte' => 'fonte',
    );
    protected $paramsApi = array(
        'user_id' =>'ID_Cliente__c',
        'last_name' =>'LastName',
        'first_name' =>'FirstName',
        'nickname' =>'Nickname__c',
        'company' =>'Nome_Societa__c',
        'codice_fiscale' =>'Codice_Fiscale__c',
        'data_di_nascita' =>'PersonBirthdate',
        'years_old' => 'Eta__c',
        'email' =>'PersonEmail',
        'telefono' =>'Phone',
        'status_subscription' =>'Stato_Utente__c',
        'studente' =>'Studente__c',
        'registration_date' =>'Data_Registrazione__c',
        'consenso_marketing' =>'Consenso_Marketing__c',
        'consenso_privacy' =>'Consenso_Privacy__c',
        'consenso_vendita' =>'Consenso_Vendita__c',
        'last_subscription_end_date' =>'Fine_Ultimo_Abbonamento__c',
        'first_subscription_start_date' =>'Inizio_Primo_Abbonamento__c',
        'membership_level' =>'Livello_Membership__c',
        'membership_level_name' =>'Nome_Livello_Membership__c',
        'billing_address_1' =>'BillingStreet',
        'billing_address_2' =>'Dettagli_Indirizzo_Fatturazione__c',
        'billing_city' =>'BillingCity',
        'billing_postcode' =>'BillingPostalCode',
        'billing_state' =>'BillingState',
        'billing_regione' =>'Regione_Fatturazione__c',
        'billing_country' =>'BillingCountry',
        'shipping_address_1' =>'ShippingStreet',
        'shipping_address_2' =>'Dettagli_Indirizzo_postale__c',
        'shipping_city' =>'ShippingCity',
        'shipping_postcode' =>'ShippingPostalCode',
        'shipping_state' =>'ShippingState',
        'shipping_regione' =>'Regione_indirizzo_postale__c',
        'shipping_country' =>'ShippingCountry',
        'select_source' =>'Seleziona_Sorgente__c', // = Shop
        'total_paid' => 'Spesa_Totale__c',
        'sex' =>'Sesso__c',
        'source' => 'Seleziona_Sorgente__c',
        'status' => 'Stato__c',
        'fonte' => 'Fonte__c',
        'external_id' => 'KEY_Cliente__c',
    );
    
    public $upsertKey = 'KEY_Cliente__c';
    public $type = 'Account';
    public $metaType = 'user';
    protected function buildItem($user_id){
        static $ifq_membership = null;
        if(is_null($ifq_membership)){
            $ifq_membership = new IFQ_Membership();
        }
        
        $user = get_user_by('ID',$user_id);
        $ifq_capabilities = get_user_meta($user_id,'ifq_capabilities',true);
        $codicefiscale = get_user_meta($user_id,'vat_number',true);
        $data = array(
            'user_id'=>$user_id,
	    'external_id' => $user_id.":SP",
            'last_name' => substr($user->last_name,0,80),
            'first_name' => substr($user->first_name,0,40),
            'nickname' => substr($user->display_name,0,40),
            'company' => $user->billing_company,
            'email' => ltrim($user->user_email,'.'),
            'billing_address_1' => $user->billing_address_1,
            'billing_address_2' => $user->billing_address_2,
            'billing_city' => $user->billing_city,
            'billing_postcode' => strlen($user->billing_postcode) > 0 ?substr($user->billing_postcode,0,20):'',
            'billing_state' => $user->billing_state,
            'billing_country' => $user->billing_country,
            'billing_regione' => '',
            'shipping_address_1' => $user->shipping_address_1,
            'shipping_address_2' => $user->shipping_address_2,
            'shipping_city' => $user->shipping_city,
            'shipping_postcode' => strlen($user->shipping_postcode) > 0 ?substr($user->shipping_postcode,0,20):'',
            'shipping_state' => $user->shipping_state,
            'shipping_country' => $user->shipping_country,
            'shipping_regione' => '',
            'telefono' =>  strlen($user->telefono) > 0 ?substr($user->telefono,0,40):'',
            'studente' => (intval(get_user_meta($user_id,'student_status',true))==1?1:0),
            'consenso_marketing' =>intval( get_user_meta($user_id,'consenso_marketing',true)),
            'consenso_vendita' => intval(get_user_meta($user_id,'consenso_vendita',true)),
            'consenso_privacy' => 1,
            'membership_level' => (!empty($ifq_capabilities['membership_level'])?$ifq_capabilities['membership_level']:0),
            'membership_level_name' => (!empty($ifq_capabilities['membership_level'])?$ifq_membership->get_membership_level_name($ifq_capabilities['membership_level']):''),
            'fonte' => 'SP',
            'source' => 'shop',
            'registration_date' => date('Y-m-d\TH:i:sP',strtotime($user->user_registered)),
            'codice_fiscale' =>  !empty($codicefiscale)? substr($codicefiscale,0,50):'',
            'total_spent' => $this->calculate_user_total_spent($user_id),
        );
        if(!$data['last_name']){
            $data['last_name'] = substr($user->billing_last_name,0,80);
        }
        if(empty($data['last_name'])){
            $data['last_name'] = '.';
        }
        if(!$data['first_name']){
            $data['first_name'] = substr($user->billing_first_name,0,40);
        }
        $subs = wcs_get_users_subscriptions($user_id);
        if(!empty($subs)){
        $start_date = '9999-99-99 99:99:99';
        $end_date = '1970-01-01 00:00:00';
            foreach($subs as $sub){
		$sub_end_date = null;
		if($sub->get_status() == 'on-hold'){
			$orders = $sub->get_related_orders('ids','all');
			if(!empty($orders) && count($orders) < 2){
				$order = wc_get_order(array_shift($orders));	
				if($order->get_status() != 'completed' && $order->get_status() !='processing'){
					$clear_dates = true;
                                        continue;
				}
			}
			elseif(!empty($orders)){	
				sort($orders);
				$order = wc_get_order(array_pop($orders));
				if($order->get_status() != 'completed' && $order->get_status() !='processing'){
				$sub_end_date = $order->order_date;
				}
			}
			else{
				$clear_dates = true; continue;
			}
		}
                $sub_start_date = $sub->get_date('start');
                if($start_date > $sub_start_date ){
                    $start_date = $sub_start_date;
                }
		if(empty($sub_end_date)){
	                $sub_end_date = $sub->get_date('end');
		}
                if(empty($sub_end_date)){
                    if($sub->get_status()=='active'){
                        $sub_end_date = $sub->get_date('next_payment');
                    }elseif($sub->get_status() =='pending-cancel'){
                        $sub_end_date = $sub->get_date('end_of_prepaid_term');
                    }
               	}
                if($sub_end_date > $end_date){
                    $end_date = $sub_end_date;
                }
            }
		
	  if($clear_dates && $start_date == '9999-99-99 99:99:99'){
		$start_date = '';
	  }
	  if($clear_dates && $end_date == '1970-01-01 00:00:00'){
		$end_date = '';
	  }
        }
        else{
            $start_date = '';
            $end_date = '';
            $data['status_subscription'] = 'mai-abbonato';
        }
	if(!empty($end_date)){
        $data['last_subscription_end_date'] = date('Y-m-d\TH:i:sP',strtotime($end_date));
	}
	if(!empty($start_date)){
        $data['first_subscription_start_date'] =date('Y-m-d\TH:i:sP',strtotime( $start_date));
        }
        if(empty($data['status_subscription'])){
            $data['status_subscription'] = strtotime($end_date) < time() ? 'ex-abbonato':'abbonato';
        }
        return $data;
        
    }
    public static function isValid($user_id){
        if(get_user_meta($user_id,'consenso_marketing',true)){
            return true;
        }
        if(get_user_meta($user_id,'consenso_vendita',true)){
            return true;
        }
        global $wpdb;
        $orders = $wpdb->get_var($wpdb->prepare("SELECT count(*) FROM {$wpdb->postmeta} WHERE meta_key='_customer_user' AND meta_value = %s",$user_id));
        if(intval($orders) > 0){
            return true;
        }
        return false;
    }
    public function calculate_user_total_spent($user_id) {
        global $wpdb;
        $total_spent = 0;
        $posts = $wpdb->get_col("SELECT pm.post_id FROM wp_postmeta pm WHERE pm.meta_key = '_customer_user' AND pm.meta_value = " . $user_id);

        if (!empty($posts)) {
            $valid_orders = $wpdb->get_col("SELECT p.ID
                FROM wp_posts p
                WHERE
                ((p.ID IN (" . implode(',', $posts) . ")
                AND p.post_type = 'shop_order') OR 
                (p.post_parent IN (" . implode(',', $posts) . ")
                AND p.post_type = 'shop_order_refund'))
                AND p.post_status IN ( 'wc-completed', 'wc-processing' );");
            if (!empty($valid_orders)) {
                $total_spent = $wpdb->get_var("SELECT SUM(CAST(pm2.meta_value AS DECIMAL(10,2)))
                    FROM wp_postmeta pm2 WHERE pm2.post_id IN(" . implode(',', $valid_orders) . ")
                    AND pm2.meta_key = '_order_total'");
            }
        }
        return $total_spent;
    }

}
IFQ_Salesforce_Item::addItemType('user','IFQ_Salesforce_User');
