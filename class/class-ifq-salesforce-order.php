<?php 
class IFQ_Salesforce_Order extends IFQ_Salesforce_Item{
    
    public $paramsCsv = array(
        'order_id' => 'order_id',
        'customer_id' => 'customer_id',
        'order_date' => 'order_date',
        'order_total' => 'order_total',
        'order_status' => 'order_status',
        'payment_method' => 'payment_method',
        'eu_vat_country' => 'eu_vat_country',
        'subscription_id' => 'subscription_id',
        'fonte' => 'fonte',
    );
    protected $paramsApi = array(
        'order_id' =>'ID_Ordine__c',
        'customer_id' =>'ID_Cliente__c',
        'order_date' =>'Data_Ordine__c',
        'order_total' =>'Totale_Ordine_custom__c',
        'order_status' =>'Status',
        'payment_method' =>'Metodo_Pagamento__c',
        'eu_vat_country' =>'Paese_di_Riferimento__c',
        'subscription_id' =>'Codice_Abbonamento__c',
        'fonte' =>'Fonte__c',
        'external_id' => 'KEY_Ordine__c',
        'salesforce_customer_id' => 'AccountId',
        'salesforce_subscription_id' => 'Abbonamento__c',
	'effective_date'=> 'EffectiveDate',
	'pricebook' => 'Pricebook2Id',
    );
    public $upsertKey = 'KEY_Ordine__c';
    public $type = 'Order';
    public $metaType = 'post';
    protected function buildItem($order_id){
        $order = wc_get_order($order_id);
        $euvat = get_post_meta($order_id,'vat_compliance_country_info',true);
        $subscription_id = wcs_get_subscriptions_for_order($order_id) ;
        if(is_array($subscription_id)){
            $subscription_id = array_shift($subscription_id);
        }
        if(is_object($subscription_id)){
            $subscription_id = $subscription_id->id;
        }
	if(!empty($subscription_id)){
            $subscriptionType = IFQ_Salesforce_Item::factory('subscription');
            $salesforceSubscriptionId = $subscriptionType->getId($subscription_id);
            
            $subscription_id.=':SP';
	}
	$productType = new IFQ_Salesforce_Product();
        $data = array(
            'order_id' => $order_id,
            'external_id' => $order_id.":SP",
            'customer_id' => $order->customer_user.":SP",
            'order_date' => date('Y-m-d\TH:i:sP',strtotime($order->order_date)),
	    'effective_date' => date('Y-m-d',strtotime($order->order_date)),
            'order_total' => $order->get_total(),
            'order_status' => $this->getStatus($order->get_status(),$order_id),
            'payment_method' => $order->payment_method_title,
            'eu_vat_country' => (isset($euvat['data'])?$euvat['data']:''),
            'fonte' => 'SP',
            'account' => '',
            'subscription_id' => $subscription_id,
	    'pricebook' =>$productType->pricebookId, 
        );
	$userType = new IFQ_Salesforce_User();
	$salesforceUserId = $userType->getId($order->customer_user);
        if(!empty($salesforceUserId)){
            $data['salesforce_customer_id'] = $salesforceUserId;
        }
        if(!empty($salesforceSubscriptionId)){
            $data['salesforce_subscription_id'] = $salesforceSubscriptionId;
        }
        return $data;
        
    }
    
    public static function isValid($order_id){
        $subscriptions = wcs_get_subscriptions_for_order($order_id,array('parent','renewal'));
        if(empty($subscriptions)){
            return true;
        }
        foreach($subscriptions as $subscription){
            if($subscription->get_status() == 'active' || $subscription->get_status() == 'pending-cancel'){
                return true;
            }
            if($subscription->get_status() == 'on-hold'){
                $end_date = $subscription->get_date('next_payment');
            }
            else{
                $end_date = $subscription->get_date('end');
            }
            if($end_date){
                $time_from_end = time() - strtotime($end_date);
                if($time_from_end < YEAR_IN_SECONDS){
                    return true;
                }
            }
        }
        return false;      
    }
    public function getStatus($status,$order_id){
        if($status!='processing' && $status!='completed'){
            return 'Bozza';
        }else{
            if(get_post_meta($order_id,'_ifq_salesforce_order_loaded',true)){
                return 'Completed';
            }
            else{
                return 'Processing';
            }
        }
    }
}
IFQ_Salesforce_Item::addItemType('order','IFQ_Salesforce_Order');
