<?php 
class IFQ_Salesforce_Product extends IFQ_Salesforce_Item{
    
    protected $recordType = array(
        'Libro' =>'0120Y000000fuGTQAY',
        'Corso/Evento' =>'0120Y000000fuGSQAY',
        'Prodotto' =>'0120Y000000fuGUQAY',
        'Abbonamento' => '0120Y000000fuGRQAY',
    );
    
    public $paramsCsv = array(
        'product_id' => 'product_id',
        'product_type' => 'product_type',
	'product_parent' => 'product_parent',
        'name' => 'name',
        'short_description' => 'short_description',
        'type' => 'type',
        'price' => 'price',
        'sales_price' => 'sales_price',
        'tax_rate' => 'tax_rate',
        'categories' => 'categories',
        'subscription_interval' => 'subscription_interval',
        'subscription_period' => 'subscription_period',
        'subscription_length' => 'subscription_length',
        'trial_period' => 'trial_period',
        'trial_length' => 'trial_length',
        'activation_price' => 'activation_price',
        'subscription_price' => 'subscription_price',
        'isbn' => 'isbn',
        'data_di_pubblicazione' => 'data_di_pubblicazione',
        'autori' => 'autori',
        'data_corso' => 'data_corso',
        'nome_sede_corso' => 'nome_sede_corso',
        'indirizzo_sede_corso' => 'indirizzo_sede_corso',
        'relatori' => 'relatori',
        'orario_corso' => 'orario_corso',
        'durata_corso' => 'durata_corso',
        'numero_massimo_partecipanti' => 'numero_massimo_partecipanti',
        'numero_minimo_partecipanti' => 'numero_minimo_partecipanti',
        'fonte' => 'fonte',
        
        
    );
    protected $paramsApi = array(
        'product_id' => 'ID_prodotto__c',
        //'product_type' => 'Tipo_Record_Prodotto__c',
        'record_type' => 'RecordTypeId',
	'product_parent' => 'Codice_Product_Parent__c',
        'name' => 'Name',
        'short_description' => 'Breve_Descrizione__c',
        'type' => 'Tipologia__c',
        'price' => 'Prezzo__c',
        'sales_price' => 'Prezzo_di_Saldo__c',
        'tax_rate' => 'Aliquota_Fiscale__c',
        'categories' => 'Categoria__c',
        'subscription_interval' => 'Intervallo_Abbonamento__c',
        'subscription_period' => 'Periodo_Abbonamento__c',
        'subscription_length' => 'Durata_Abbonamento__c',
        'trial_period' => 'Periodo_di_Prova__c',
        'trial_length' => 'Durata_Periodo_di_Prova__c',
        'activation_price' => 'Prezzo_Attivazione__c',
        'subscription_price' => 'Prezzo_Abbonamento__c',
        'isbn' => 'ISBN__c',
        'data_di_pubblicazione' => 'Data_di_Pubblicazione__c',
        'autori' => 'Autori__c',
        'data_corso' => 'Data_Corso__c',
        'nome_sede_corso' => 'Nome_Sede_Corso__c',
        'indirizzo_sede_corso' => 'Indirizzo_Sede_Corso__c',
        'relatori' => 'Relatori__c',
        'orario_corso' => 'Orario_Corso__c',
        'durata_corso' => 'Durata_Corso__c',
        'numero_massimo_partecipanti' => 'Numero_Massimo_Partecipanti__c',
        'numero_minimo_partecipanti' => 'Numero_Minimo_Partecipanti__c',
        'fonte' => 'Fonte__c',
        'external_id' =>'KEY_Prodotto__c',
    );
    public $upsertKey = 'KEY_Prodotto__c';
    public $type = 'Product2';
    public $metaType = 'post';
    public $pricebookId = "01s0Y000004bwZiQAI"; 
    protected function buildItem($product_id){
        $product = wc_get_product($product_id);
        $product_type = 'Prodotto';
        if(has_term('abbonamenti', 'product_cat', $product_id) && $product->get_type() == 'subscription'){
            $product_type = 'Abbonamento';
            $cdata = array(
                'subscription_interval' => $product->subscription_period_interval,
                'subscription_period' =>  $product->subscription_period ,
                'subscription_length' => $product->subscription_length,
                'trial_period' => $product->subscription_trial_period,
                'trial_lenght' => $product->subscription_trial_length,
                'activation_price' => $product->get_sign_up_fee_including_tax(),
                'subscription_price' => $product->subscription_price,
            );
        }elseif(has_term('libri-e-dvd', 'product_cat', $product_id)){
            $product_type = 'Libro';
            $cdata = array(
                'isbn' => get_post_meta($product_id,'codice_isbn',true),
                'data_di_pubblicazione' =>  get_post_meta($product_id,'anno_di_pubblicazione',true),
            );
        }elseif(has_term(array('corsi','eventi'), 'product_cat', $product_id)){
            $product_type = 'Corso/Evento';
            $relatori = get_post_meta($product_id,'relatore',true);
            $relatori_name = array();
            if(is_array($relatori) && !empty($relatori)){
                foreach($relatori as $user_id ){
                       if(is_numeric($user_id)){
                               $user = get_userdata( $user_id );
                       $relatori_name[] = $user->display_name;
                       }
               }
               $relatori = $relatori_name;
            }
            $cdata = array(
                'data_corso' => get_post_meta($product_id,'data_evento',true),
                'nome_sede_corso' => get_post_meta($product_id,'sede_del_corso',true),
                'indirizzo_sede_corso' => get_post_meta($product_id,'indirizzo',true),
                'relatori' => implode(',',$relatori_name),
                'orario_corso' => get_post_meta($product_id,'orario',true),
                'durata_corso' => get_post_meta($product_id,'durata',true),
                'numero_massimo_partecipanti' => get_post_meta($product_id,'max_partecipanti',true),
                'numero_minimo_partecipanti' => get_post_meta($product_id,'min_partecipanti',true),
            );
        }
        $tax_rate_info =array_shift( WC_Tax::get_base_tax_rates($product->get_tax_class()));
        $data = array(
            'product_id' => $product_id,
	    'external_id' => $product_id.':SP',
            'product_type' => $product_type,
            'record_type' => $this->recordType[$product_type],
            'product_parent' => !empty($product->post->post_parent)?$product->post->post_parent.':SP':'',
            'name' => $product->get_title(),
            'short_description'=> str_ireplace(array("\n","\r","\x0D"), " ",esc_attr(strip_tags($product->post->post_excerpt))),
            'type' =>  $product->get_type(),
            'price' => $product->get_price(),
            'sales_price' => $product->get_sale_price(),
            'tax_rate' => number_format($tax_rate_info['rate'],2), // TODO get %
            'fonte' => 'SP',
        );
        if(empty($data['sales_price'])){
                $data['sales_price'] = $data['price'];
        }

        if(!empty($cdata)){
            $data = array_merge($data,$cdata);
        }
        return $data;
        
    }
    public function getPricebookEntryId($item_id){
	$Id = get_metadata($this->metaType,$item_id,'_ifq_salesforce_pricebook_entry_id',true);
	if(empty($Id)){
            $sforce = IFQ_Salesforce_Update::instance();
            $res = $sforce->query("SELECT Id FROM PricebookEntry WHERE Product2Id='".$this->getId($item_id)."'");
            if(!empty($res)&&is_array($res)){
                $Id = $res[0]->Id;
                update_metadata($this->metaType,$item_id,'_ifq_salesforce_pricebook_entry_id',$Id);
            }else{
                $Id = $this->updatePricebookEntry($item_id);
            }
	}
        return $Id;
    }
    
    protected function updatePricebookEntry($item_id){
        $productId = $this->getId($item_id);
	$product = wc_get_product($item_id);
        $Id = false;
        if(!empty($productId)){
            $data = array(
                'Product2Id' => $productId,
                'Pricebook2Id' => $this->pricebookId,
                'IsActive' => true,
                'UnitPrice' => $product->get_price(),
                
            );
            $pricebookEntryItemType = new IFQ_Salesforce_PricebookEntry();
            $sforce = IFQ_Salesforce_Update::instance(); var_dump($pricebookEntryItemType->buildItem($data));
            $sforce->upsert($pricebookEntryItemType->buildItem($data),$pricebookEntryItemType);
            $res = $sforce->query("SELECT Id FROM PricebookEntry WHERE Product2Id='".$productId."' ");
            if(!empty($res)&&is_array($res)){
                $Id = $res[0]->Id;
                update_metadata($this->metaType,$item_id,'_ifq_salesforce_pricebook_entry_id',$Id);
            }
        }
        return $Id;
    }
}
IFQ_Salesforce_Item::addItemType('product','IFQ_Salesforce_Product');


class IFQ_Salesforce_PricebookEntry extends IFQ_Salesforce_Item{
    
    
    public $upsertKey = 'Id';
    public $type = 'PricebookEntry';
    public function buildItem($data) {
        if(is_array($data)){
            return (object) $data;
        }
    }
}
