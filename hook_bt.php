<?php
require_once('vendor/autoload.php');

$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();

// Set your secret key: remember to change this to your live secret key in production
// See your keys here: https://dashboard.stripe.com/account/apikeys
Braintree\Configuration::environment(getenv('BRAINTREE_ENVIROMENT'));
Braintree\Configuration::merchantId(getenv('BRAINTREE_MERCHANTID'));
Braintree\Configuration::publicKey(getenv('BRAINTREE_PUBLIC_KEY'));
Braintree\Configuration::privateKey(getenv('BRAINTREE_SECRET_KEY'));


$bt_challenge = "";
if(isset($_GET['bt_challenge']))
{
    $bt_challenge = $_GET['bt_challenge'];
}
if(isset($bt_challenge) && $bt_challenge != ""){
    echo \Braintree\WebhookNotification::verify($bt_challenge);
}

if(
    isset($_POST["bt_signature"]) &&
    isset($_POST["bt_payload"])
  ) {
      $webhookNotification = \Braintree\WebhookNotification::parse(
          $_POST["bt_signature"], $_POST["bt_payload"]
      );
      $message =
          "[Webhook Received " . $webhookNotification->timestamp->format('Y-m-d H:i:s') . "] "
          . "Kind: " . $webhookNotification->kind . " | "
          . "subscription_id: " . $webhookNotification->subscription->id . " | "
          . "plan_id: " . $webhookNotification->subscription->planId . " | "
          . "billingPeriodStartDate: " . $webhookNotification->subscription->billingPeriodStartDate . " | "
          . "billingPeriodEndDate: " . $webhookNotification->subscription->billingPeriodEndDate . " | "
          . "customer_id: " . $webhookNotification->subscription->transactions[0]->customerDetails->id . " | "
          . "customer_email: " . $webhookNotification->subscription->transactions[0]->customerDetails->email . " | "
          . "amount: " . $webhookNotification->subscription->transactions[0]->amount . " | "
          . "CreditCard_id: " . $webhookNotification->subscription->transactions[0]->creditCardDetails->uniqueNumberIdentifier . " | "
          . "Subscription: " . $webhookNotification->subscription->__toString() . "\n";
      file_put_contents("./webhook.l", $message, FILE_APPEND);
  }

// http_response_code(200); // PHP 5.4 or greater


 ?>
